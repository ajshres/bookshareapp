export default function(server) {
  server.loadFixtures();
  
  //Create 10 users
  let users = server.createList('user', 10);
  //For first five users, create 3 books
  for(let i = 0; i < 5; ++i) {
    const user = users[i];
    let books = server.createList('book', 3, {user});

    //Make each of the other 5 users send borrow requests
    books.forEach(function(book) {
      const bUser = users[i+5];
      server.create('request', {book, requestedBy: bUser});
    });
  }
}