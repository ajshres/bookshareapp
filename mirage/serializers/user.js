import { RestSerializer } from 'ember-cli-mirage';

export default RestSerializer.extend({
    attrs: ['id', 'username', 'name']
});