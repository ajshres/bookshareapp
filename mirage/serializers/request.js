import { RestSerializer } from 'ember-cli-mirage';

export default RestSerializer.extend({
    include: ['book', 'requestedBy'],
    embed: false
});
