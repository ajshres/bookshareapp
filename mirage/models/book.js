import { Model, hasMany, belongsTo } from 'ember-cli-mirage';

export default Model.extend({
    requests: hasMany('request'),
    user: belongsTo('user')
});