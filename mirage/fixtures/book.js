export default [
    {id: 1, title: 'Inferno', author: 'Dan Brown', publisher: 'Doubleday', language: 'English', genre: 'Mystery', publicationDate: '05/14/2013', loginid: 'jagat', available: false, requested: false},
    {id: 2, title: 'Five Point Someone 3 Idiots', author: 'Chetan Bhagat', publisher: 'Rupa & Co', language: 'Hindi', genre: 'Fiction', publicationDate: '08/15/2004', loginid: 'tuladhar', available: true, requested: true},
    {id: 3, title: 'Muna Madan', author: 'Laxmi Prasad Devkota', publisher: 'None', language: 'Nepali', genre: 'Romance', publicationDate: '08/15/2000', loginid: 'jagat', available: true, requested: false},
    {id: 4, title: 'In Search of Lost Time', author: 'Marcel Proust', publisher: 'None', language: 'English', genre: 'Modern Literature', publicationDate: '05/14/1913', loginid: 'jagat', available: false, requested: true},
];
