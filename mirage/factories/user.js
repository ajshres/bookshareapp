import { Factory, faker } from 'ember-cli-mirage';

export default Factory.extend({
    name: faker.name.findName,
    username: faker.name.firstName,
    password: faker.name.lastName
});
