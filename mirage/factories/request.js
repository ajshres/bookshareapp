import { Factory, association, faker } from 'ember-cli-mirage';

export default Factory.extend({
    book: association(),
    requestedBy: association(),
    approved: faker.random.boolean,
    declined: function() {
        if(this.approved) return false;
        else return faker.random.boolean();
    },
    verify: faker.random.boolean,
    lend: faker.random.boolean
});
