import { Factory, association, faker } from 'ember-cli-mirage';

export default Factory.extend({
    title: function() {
        return faker.lorem.word() + ' ' + faker.lorem.word() + ' ' + faker.lorem.word();
    },
    author: faker.name.firstName,
    publisher: faker.name.lastName,
    language: faker.lorem.word,
    genre: faker.lorem.word,
    user: association(),
    publicationDate: faker.date.past,
    available: faker.random.boolean,
    requested: faker.random.boolean
});
