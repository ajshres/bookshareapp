import Mirage from 'ember-cli-mirage';

export default function() {
  //this.urlPrefix = 'localhost:4200';
  
  /***********************
   * USERS
   **********************/
  
  //Signup
  this.post('users', function(db, request) {
    let attrs = this.normalizedRequestAttrs();
    return db.db.users.insert(attrs);
  });

  //Gets information on a single user
  this.get('users/:id', function(db, request) {
    return db.users.findBy({username: request.params.id});
  });

  this.post('/token', function(db, request){
    var uname = request.requestBody.split('&')[1].split('=')[1];
    var pass = request.requestBody.split('&')[2].split('=')[1];
    
    var foundUser = db.users.findBy({
      username: uname,
      password: pass
    });
    
    if (foundUser) {
      return {
        access_token: 'secretcode'
      }
    }
    else {
      return new Mirage.Response(400, {}, {
        error: {
          status: 400,
          message: "Invalid username or password",
          error: 'invalid_grant'
        }
      });
    }
  });
  
  /***********************
   * BOOKS
   **********************/
  
  //Gets information on a specific book
  this.get('books/:id');


  this.get('books', function(schema, request) {
    if(request.queryParams.title !== undefined) {
      let filteredBooks = schema.db.books.filter(function(i) {
        return i.title.toLowerCase().indexOf(request.queryParams.title.toLowerCase()) !== -1;
      });
      return { books: filteredBooks };
    } else {
      return schema.books.all();
    }
  });

  this.post('books', function(schema, request) {
    let attrs = this.normalizedRequestAttrs();
    return schema.db.books.insert(attrs);
  });

  this.put('books/:id', function(schema, request) {
    let attrs = this.normalizedRequestAttrs();
    return schema.db.books.update(request.params.id, attrs);
  });

  this.del('books/:id');

  this.get('user');
  this.get('book');

  this.get('signup');

  /***********************
   * REQUESTS
   **********************/
  this.get('requests', function(schema, request) {
    return schema.requests.all();
  });

  this.post('requests', function(schema, request) {
    let attrs = this.normalizedRequestAttrs();
    var result = schema.db.requests.insert({
      requestedBy: 2,
      book: attrs.book_id
    });
    return {request: result};
  });

  this.put('requests/:id', function(schema, request){
    let attrs = this.normalizedRequestAttrs();
    console.log(attrs);
    let post = schema.requests.find(request.params.id);
    var p = post.update({
      approved: attrs.aproved,
      decline: attrs.decline,
      lend: attrs.lend,
      recieved: attrs.recieved
    });
    return p;



  });
}