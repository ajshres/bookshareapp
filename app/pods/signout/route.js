import Ember from 'ember';

export default Ember.Route.extend({

    creds: Ember.inject.service('logins'),

    beforeModel() {
        console.log("Signout");
        this.get('creds').logoutsession();
        this.transitionTo('index');
    }
});
