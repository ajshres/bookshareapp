import { A } from '@ember/array';
import Mixin from '@ember/object/mixin';
import { computed } from '@ember/object';
import { isEmpty } from '@ember/utils';
import { inject as service } from '@ember/service';
import Table from 'ember-light-table';
import { task, timeout } from 'ember-concurrency';
import Component from '@ember/component';

export default Component.extend({
    showFilter: false,

    classNames: ['book-table'],
    
    enableSync: true,

    store: service(),

    //The columns in the table (Passed by property)
    columns: null,

    //The page that we should load
    page: 0,
    //The number of records to load on each page
    limit: 10,

    //The field to sort by
    sort: 'title',
    //The direction of sorting
    dir: 'asc',

    //Flag to determine if something is loading
    isLoading: computed.or('isWorking', 'fetchRecords.isRunning', 'filterAndSort.isRunning'),

    isWorking: false,

    //Working/Loading flag passed through property
    isWorking: false,

    //The model that is currently being displayed
    dataModel: [],

    //Computed property which returns the sorted model by 'sort' and 'dir'
    sortedModel: computed.sort('dataModel', 'sortBy').readOnly(),

    //Returns the parameters to sort by
    sortBy: computed('dir', 'sort', function() {
        return [`${this.get('sort')}:${this.get('dir')}`];
    }).readOnly(),

    //The meta data obtained about the current model
    meta: null,

    //Reference to the table we are currently displaying
    table: null,

    //The query string parameters to send (Passed by property)
    query: {},

    //The model whose records we are fetching
    model: '',
    
    //The text entered in the filter text field
    filterQuery: '',

    init() {
        this._super(...arguments);
        let table = new Table(this.get('columns'), A([]),{ enableSync: this.get('enableSync') });
        let sortColumn = table.get('allColumns').findBy('valuePath', this.get('sort'));

        // Setup initial sort column
        if (sortColumn) sortColumn.set('sorted', true);

        this.set('table', table);
        this.get('fetchRecords').perform();
    },

    filterAndSort: task(function* () {
        const table = this.get('table');
        const q = this.get('filterQuery');

        //Sort the result
        const sortedModel = this.get('sortedModel');

        let result = sortedModel;

        //Filter the result
        if (q !== '') {
            result = sortedModel.filter((m) => {
                return m.get('title').toLowerCase().includes(q.toLowerCase());
            });
        }

        table.setRows([]);
        yield timeout(100);
        table.setRows(result);
    }).restartable(),

    fetchRecords: task(function*() {
        let requestQuery = Object.assign(this.getProperties(['page', 'limit']), this.get('query'));

        let records = yield this.get('store')
            .query(this.get('model'), requestQuery);

        this.set('dataModel', records);

        yield this.get('filterAndSort').perform();
    }).restartable(),

    actions: {
        onColumnClick(column) {
            if(column.sorted) {
                this.setProperties({
                    dir: column.ascending ? 'asc' : 'desc',
                    sort: column.get('valuePath')
                });
                this.get('filterAndSort').perform();
            }
        },

        loadPrevPage() {
            if(this.get('page') > 0)  {
                this.decrementProperty('page');
                this.get('fetchRecords').perform();
            }
        },

        loadNextPage() {
            this.incrementProperty('page');
            this.get('fetchRecords').perform();
        },

        onSearchChange(query) {
            this.get('filterAndSort').perform();
        }
    }
});