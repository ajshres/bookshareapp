import Ember from 'ember';

const BookListItem = Ember.Component.extend({
    classNames:['panel panel-info'],

    title: Ember.computed('book.title', function() {
        const title = this.get('book.title');
        if (!title) {
            return "No Title Available";
        }
        else {
            return title;
        }
    })

    
});

BookListItem.reopenClass({
    positionalParams: ['book'],
});


export default BookListItem;
