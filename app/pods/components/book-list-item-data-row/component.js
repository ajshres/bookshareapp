import Ember from 'ember';

const BookListItemDataRow = Ember.Component.extend({});

BookListItemDataRow.reopenClass({
    positionalParams: ['key', 'value']
});

export default BookListItemDataRow;
