import Ember from 'ember';

const cred = {};

export default Ember.Service.extend({
    
    credentials(cred) {
        this.set('creds', cred);
        console.log(this.get('creds'));
        this.setsessiondata(cred);
    },

    setsessiondata(cred) {
        sessionStorage.setItem('sessionlogin', cred);
        return true;
    },

    getsessiondata() {
        return sessionStorage.getItem('sessionlogin');
    },

    logoutsession() {
        this.set('creds', '');
        sessionStorage.clear();
        return true;
    }
});
