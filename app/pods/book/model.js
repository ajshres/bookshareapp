import DS from 'ember-data';

export default DS.Model.extend({
    title: DS.attr('string'),
    author: DS.attr('string'),
    publisher: DS.attr('string'),
    user: DS.belongsTo('user'),
    language: DS.attr('string'),
    genre: DS.attr('string'),
    publicationDate: DS.attr('string'),
    available: DS.attr('boolean'),
    requested: DS.attr('boolean')
});
