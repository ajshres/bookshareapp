import DS from 'ember-data';

export default DS.Model.extend({
    book: DS.belongsTo('book'),
    requestedBy: DS.belongsTo('user'),
    approved: DS.attr('boolean'),
    declined: DS.attr('boolean'),
    lend: DS.attr('boolean'),
    recieved: DS.attr('boolean')    
});
