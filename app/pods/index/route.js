import Ember from 'ember';

export default Ember.Route.extend({

    session: Ember.inject.service('session'),

    beforeModel() {
        //Redirect to books if the user is already authenticated
        if(this.get('session').get('isAuthenticated'))
            this.replaceWith('books');
    },

    actions: {
        authenticate(identification, password) {
            this.get('session').authenticate('authenticator:oauth2', identification, password).then(() => {
                this.transitionTo('books.my');
            }).catch((reason) => {
              this.get('controller').set('errorMessage', reason.error || reason);
            });
        }
    }
});
