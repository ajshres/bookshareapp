import DS from 'ember-data';

export default DS.RESTAdapter.extend({
    login(username, password) {
        return new Ember.RSVP.Promise(function(resolve, reject) {
            Ember.$.ajax({
                type: 'POST',
                dataType: 'json',
                url: '/users/login',
                data: {
                    username: username,
                    password: password
                }
            }).then(function(data) {
                Ember.run(null, resolve, data);
            }, function(jqXHR) {
                jqXHR.then = null;
                Ember.run(null, reject, jqXHR);
            });
        });
    }
});
