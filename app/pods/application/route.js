import Ember from 'ember';

export default Ember.Route.extend({
    session: Ember.inject.service('session'),

    beforeModel(transition) {
        //Transition to root if not logged in
        if(!this.get('session').get('isAuthenticated'))
            this.transitionTo('index');
    },

    actions: {
        logout() {
            this.get('session').invalidate();
            this.transitionTo('index');
        }
    }
});
