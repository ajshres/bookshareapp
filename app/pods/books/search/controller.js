import Ember from 'ember';

export default Ember.Controller.extend({
    columns: Ember.computed(function() {
        return [
            {
                label: 'Title',
                valuePath: 'title'
            },
            {
                label: 'Author',
                valuePath: 'author'
            },
            {
                label: 'Language',
                valuePath: 'language'
            },
            {
                label: 'Genre',
                valuePath: 'genre'
            },
            {
                label: 'Publisher',
                valuePath: 'publisher'
            },
            {
                label: 'Published On',
                valuePath: 'publicationDate'
            },
            {
                label: 'Actions',
                sortable: false,
                cellComponent: 'book-search-actions'
            }
        ];
    }),

    notifications: Ember.inject.service('notification-messages'),

    working: false,

    actions: {
        requestBook(book) {
            book.set('requesting', true);
            let request = this.store.createRecord('request', {
                book_id: book.get('id')
            });

            request.save()
                .then(() => {
                    book.set('requested', true);
                    this.get('notifications').success(`Your request has been sent to the owner of '${book.get('title')}'`, {
                        autoClear: true
                    });
                })
                .catch(() => {
                    book.get('notifications').success("Sorry! We could not complete your request. Please try again later", {
                        autoClear: true
                    });
                })
                .finally(() => {book.set('requesting', false)})
        }
    }
});
