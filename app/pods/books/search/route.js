import Ember from 'ember';

export default Ember.Route.extend({
    model(params) {
        return {q: params.query};
    },

    setupController(controller, model) {
        controller.set('title', this.controllerFor('books').get('title'));
    }
});
