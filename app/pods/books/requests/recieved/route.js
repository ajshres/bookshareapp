import Ember from 'ember';

export default Ember.Route.extend({
    model() {
        return this.store.findAll('request');
    },

    actions: {
        approve(request) {
            request.set('approved', true);
            request.save();
        },
        decline(request) {
            request.set('declined', true);
            request.save();
        },
        lend(request){
            request.set('lend', true);
            request.save();
        }
    }
});
