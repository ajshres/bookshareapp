import Ember from 'ember';

export default Ember.Controller.extend({
    columns: Ember.computed(function() {
        return [
            {
                label: 'Title',
                valuePath: 'book.title'
            },
            {
                label: 'Author',
                valuePath: 'book.author'
            },
            {
                label: 'Published On',
                valuePath: 'book.publicationDate'
            },
            {
                label: 'Requested By',
                valuePath: 'requestedBy.username'
            },
            // {
            //     label: 'Actions',
            //     sortable: false,
            //     cellComponent: 'received-requests-actions'
            // }
        ];
    }),
});