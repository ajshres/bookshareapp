import Ember from 'ember';

export default Ember.Controller.extend({
    columns: Ember.computed(function() {
        return [
            {
                label: 'Title',
                valuePath: 'title'
            },
            {
                label: 'Author',
                valuePath: 'author'
            },
            {
                label: 'Language',
                valuePath: 'language'
            },
            {
                label: 'Genre',
                valuePath: 'genre'
            },
            {
                label: 'Publisher',
                valuePath: 'publisher'
            },
            {
                label: 'Published On',
                valuePath: 'publicationDate'
            },
            {
                label: 'Actions',
                sortable: false,
                cellComponent: 'my-book-actions'
            }
        ];
    }),

    notifications: Ember.inject.service('notification-messages'),

    working: false,

    actions: {
        deleteBook: function(row, table) {
            row.set('deleting', true);       
            row.get('content').destroyRecord()
                .then(() => {
                    table.removeRow(row);
                    this.get('notifications').success("Your book has been deleted successfully", {
                        autoClear: true
                    });
                }).catch(() => {
                    this.get('notifications').error("Sorry! We could not delete your book. Please try again later.", {
                        autoClear: true
                    });
                }).finally(() => {
                    row.set('deleting', false);
                });
        }
    }
});
