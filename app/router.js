import Ember from 'ember';
import config from './config/environment';

const Router = Ember.Router.extend({
  location: config.locationType,
  rootURL: config.rootURL
});

Router.map(function() {
  this.route('users', function() {
    this.route('new', {'path': '/signup'});
  });

  this.route('books', function() {
    this.route('new');
    this.route('my');
    this.route('update', {path: '/update/:book_id'});

    this.route('search');
    this.route('requests', function() {
      this.route('sent');
      this.route('recieved');
    });
  });

  this.route('notfound', {path: '*path'});
  this.route('signout');
  this.route('settings');
});

export default Router;
